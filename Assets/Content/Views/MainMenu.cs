#region Using Statements
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
#endregion

namespace Delight
{
    public partial class MainMenu
    {
 
        public void Play()
        {
            SubmenuSwitcher.SwitchTo(LevelSelectWindow);
        }

        public void ShowOptions()
        {
            Debug.Log("Options Clicked");
        }

        public void Quit()
        {
            Debug.Log("Quit clicked");
        }

        public void LevelSelectNavigateBack()
        {
            SubmenuSwitcher.SwitchTo(MainMenuWindow);
        }

    }
}
