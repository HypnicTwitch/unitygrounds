// Internal view logic generated from "MainMenu.xml"
#region Using Statements
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
#endregion

namespace Delight
{
    public partial class MainMenu : UIView
    {
        #region Constructors

        public MainMenu(View parent, View layoutParent = null, string id = null, Template template = null, bool deferInitialization = false) :
            base(parent, layoutParent, id, template ?? MainMenuTemplates.Default, deferInitialization)
        {
            if (deferInitialization)
                return;

            // constructing ViewSwitcher (SubmenuSwitcher)
            SubmenuSwitcher = new ViewSwitcher(this, this, "SubmenuSwitcher", SubmenuSwitcherTemplate);
            MainMenuWindow = new Region(this, SubmenuSwitcher.Content, "MainMenuWindow", MainMenuWindowTemplate);
            Image1 = new Image(this, MainMenuWindow.Content, "Image1", Image1Template);
            Label1 = new Label(this, MainMenuWindow.Content, "Label1", Label1Template);
            Group1 = new Group(this, MainMenuWindow.Content, "Group1", Group1Template);
            Button1 = new Button(this, Group1.Content, "Button1", Button1Template);
            Button1.Click.RegisterHandler(this, "Play");
            Button2 = new Button(this, Group1.Content, "Button2", Button2Template);
            Button2.Click.RegisterHandler(this, "ShowOptions");
            Button3 = new Button(this, Group1.Content, "Button3", Button3Template);
            Button3.Click.RegisterHandler(this, "Quit");
            LevelSelectWindow = new LevelSelectExample(this, SubmenuSwitcher.Content, "LevelSelectWindow", LevelSelectWindowTemplate);
            LevelSelectWindow.NavigateBack.RegisterHandler(this, "LevelSelectNavigateBack");
            this.AfterInitializeInternal();
        }

        public MainMenu() : this(null)
        {
        }

        static MainMenu()
        {
            var dependencyProperties = new List<DependencyProperty>();
            DependencyProperties.Add(MainMenuTemplates.Default, dependencyProperties);

            dependencyProperties.Add(SubmenuSwitcherProperty);
            dependencyProperties.Add(SubmenuSwitcherTemplateProperty);
            dependencyProperties.Add(MainMenuWindowProperty);
            dependencyProperties.Add(MainMenuWindowTemplateProperty);
            dependencyProperties.Add(Image1Property);
            dependencyProperties.Add(Image1TemplateProperty);
            dependencyProperties.Add(Label1Property);
            dependencyProperties.Add(Label1TemplateProperty);
            dependencyProperties.Add(Group1Property);
            dependencyProperties.Add(Group1TemplateProperty);
            dependencyProperties.Add(Button1Property);
            dependencyProperties.Add(Button1TemplateProperty);
            dependencyProperties.Add(Button2Property);
            dependencyProperties.Add(Button2TemplateProperty);
            dependencyProperties.Add(Button3Property);
            dependencyProperties.Add(Button3TemplateProperty);
            dependencyProperties.Add(LevelSelectWindowProperty);
            dependencyProperties.Add(LevelSelectWindowTemplateProperty);
        }

        #endregion

        #region Properties

        public readonly static DependencyProperty<ViewSwitcher> SubmenuSwitcherProperty = new DependencyProperty<ViewSwitcher>("SubmenuSwitcher");
        public ViewSwitcher SubmenuSwitcher
        {
            get { return SubmenuSwitcherProperty.GetValue(this); }
            set { SubmenuSwitcherProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Template> SubmenuSwitcherTemplateProperty = new DependencyProperty<Template>("SubmenuSwitcherTemplate");
        public Template SubmenuSwitcherTemplate
        {
            get { return SubmenuSwitcherTemplateProperty.GetValue(this); }
            set { SubmenuSwitcherTemplateProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Region> MainMenuWindowProperty = new DependencyProperty<Region>("MainMenuWindow");
        public Region MainMenuWindow
        {
            get { return MainMenuWindowProperty.GetValue(this); }
            set { MainMenuWindowProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Template> MainMenuWindowTemplateProperty = new DependencyProperty<Template>("MainMenuWindowTemplate");
        public Template MainMenuWindowTemplate
        {
            get { return MainMenuWindowTemplateProperty.GetValue(this); }
            set { MainMenuWindowTemplateProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Image> Image1Property = new DependencyProperty<Image>("Image1");
        public Image Image1
        {
            get { return Image1Property.GetValue(this); }
            set { Image1Property.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Template> Image1TemplateProperty = new DependencyProperty<Template>("Image1Template");
        public Template Image1Template
        {
            get { return Image1TemplateProperty.GetValue(this); }
            set { Image1TemplateProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Label> Label1Property = new DependencyProperty<Label>("Label1");
        public Label Label1
        {
            get { return Label1Property.GetValue(this); }
            set { Label1Property.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Template> Label1TemplateProperty = new DependencyProperty<Template>("Label1Template");
        public Template Label1Template
        {
            get { return Label1TemplateProperty.GetValue(this); }
            set { Label1TemplateProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Group> Group1Property = new DependencyProperty<Group>("Group1");
        public Group Group1
        {
            get { return Group1Property.GetValue(this); }
            set { Group1Property.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Template> Group1TemplateProperty = new DependencyProperty<Template>("Group1Template");
        public Template Group1Template
        {
            get { return Group1TemplateProperty.GetValue(this); }
            set { Group1TemplateProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Button> Button1Property = new DependencyProperty<Button>("Button1");
        public Button Button1
        {
            get { return Button1Property.GetValue(this); }
            set { Button1Property.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Template> Button1TemplateProperty = new DependencyProperty<Template>("Button1Template");
        public Template Button1Template
        {
            get { return Button1TemplateProperty.GetValue(this); }
            set { Button1TemplateProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Button> Button2Property = new DependencyProperty<Button>("Button2");
        public Button Button2
        {
            get { return Button2Property.GetValue(this); }
            set { Button2Property.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Template> Button2TemplateProperty = new DependencyProperty<Template>("Button2Template");
        public Template Button2Template
        {
            get { return Button2TemplateProperty.GetValue(this); }
            set { Button2TemplateProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Button> Button3Property = new DependencyProperty<Button>("Button3");
        public Button Button3
        {
            get { return Button3Property.GetValue(this); }
            set { Button3Property.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Template> Button3TemplateProperty = new DependencyProperty<Template>("Button3Template");
        public Template Button3Template
        {
            get { return Button3TemplateProperty.GetValue(this); }
            set { Button3TemplateProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<LevelSelectExample> LevelSelectWindowProperty = new DependencyProperty<LevelSelectExample>("LevelSelectWindow");
        public LevelSelectExample LevelSelectWindow
        {
            get { return LevelSelectWindowProperty.GetValue(this); }
            set { LevelSelectWindowProperty.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Template> LevelSelectWindowTemplateProperty = new DependencyProperty<Template>("LevelSelectWindowTemplate");
        public Template LevelSelectWindowTemplate
        {
            get { return LevelSelectWindowTemplateProperty.GetValue(this); }
            set { LevelSelectWindowTemplateProperty.SetValue(this, value); }
        }

        #endregion
    }

    #region Data Templates

    public static class MainMenuTemplates
    {
        #region Properties

        public static Template Default
        {
            get
            {
                return MainMenu;
            }
        }

        private static Template _mainMenu;
        public static Template MainMenu
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenu == null || _mainMenu.CurrentVersion != Template.Version)
#else
                if (_mainMenu == null)
#endif
                {
                    _mainMenu = new Template(UIViewTemplates.UIView);
#if UNITY_EDITOR
                    _mainMenu.Name = "MainMenu";
                    _mainMenu.LineNumber = 0;
                    _mainMenu.LinePosition = 0;
#endif
                    Delight.MainMenu.SubmenuSwitcherTemplateProperty.SetDefault(_mainMenu, MainMenuSubmenuSwitcher);
                    Delight.MainMenu.MainMenuWindowTemplateProperty.SetDefault(_mainMenu, MainMenuMainMenuWindow);
                    Delight.MainMenu.Image1TemplateProperty.SetDefault(_mainMenu, MainMenuImage1);
                    Delight.MainMenu.Label1TemplateProperty.SetDefault(_mainMenu, MainMenuLabel1);
                    Delight.MainMenu.Group1TemplateProperty.SetDefault(_mainMenu, MainMenuGroup1);
                    Delight.MainMenu.Button1TemplateProperty.SetDefault(_mainMenu, MainMenuButton1);
                    Delight.MainMenu.Button2TemplateProperty.SetDefault(_mainMenu, MainMenuButton2);
                    Delight.MainMenu.Button3TemplateProperty.SetDefault(_mainMenu, MainMenuButton3);
                    Delight.MainMenu.LevelSelectWindowTemplateProperty.SetDefault(_mainMenu, MainMenuLevelSelectWindow);
                }
                return _mainMenu;
            }
        }

        private static Template _mainMenuSubmenuSwitcher;
        public static Template MainMenuSubmenuSwitcher
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSubmenuSwitcher == null || _mainMenuSubmenuSwitcher.CurrentVersion != Template.Version)
#else
                if (_mainMenuSubmenuSwitcher == null)
#endif
                {
                    _mainMenuSubmenuSwitcher = new Template(ViewSwitcherTemplates.ViewSwitcher);
#if UNITY_EDITOR
                    _mainMenuSubmenuSwitcher.Name = "MainMenuSubmenuSwitcher";
                    _mainMenuSubmenuSwitcher.LineNumber = 2;
                    _mainMenuSubmenuSwitcher.LinePosition = 4;
#endif
                }
                return _mainMenuSubmenuSwitcher;
            }
        }

        private static Template _mainMenuMainMenuWindow;
        public static Template MainMenuMainMenuWindow
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuMainMenuWindow == null || _mainMenuMainMenuWindow.CurrentVersion != Template.Version)
#else
                if (_mainMenuMainMenuWindow == null)
#endif
                {
                    _mainMenuMainMenuWindow = new Template(RegionTemplates.Region);
#if UNITY_EDITOR
                    _mainMenuMainMenuWindow.Name = "MainMenuMainMenuWindow";
                    _mainMenuMainMenuWindow.LineNumber = 4;
                    _mainMenuMainMenuWindow.LinePosition = 6;
#endif
                }
                return _mainMenuMainMenuWindow;
            }
        }

        private static Template _mainMenuImage1;
        public static Template MainMenuImage1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuImage1 == null || _mainMenuImage1.CurrentVersion != Template.Version)
#else
                if (_mainMenuImage1 == null)
#endif
                {
                    _mainMenuImage1 = new Template(ImageTemplates.Image);
#if UNITY_EDITOR
                    _mainMenuImage1.Name = "MainMenuImage1";
                    _mainMenuImage1.LineNumber = 5;
                    _mainMenuImage1.LinePosition = 8;
#endif
                    Delight.Image.SpriteProperty.SetDefault(_mainMenuImage1, Assets.Sprites["MainMenuDemoBg"]);
                    Delight.Image.HeightProperty.SetDefault(_mainMenuImage1, new ElementSize(480f, ElementSizeUnit.Pixels));
                    Delight.Image.PreserveAspectProperty.SetDefault(_mainMenuImage1, true);
                }
                return _mainMenuImage1;
            }
        }

        private static Template _mainMenuLabel1;
        public static Template MainMenuLabel1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLabel1 == null || _mainMenuLabel1.CurrentVersion != Template.Version)
#else
                if (_mainMenuLabel1 == null)
#endif
                {
                    _mainMenuLabel1 = new Template(LabelTemplates.Label);
#if UNITY_EDITOR
                    _mainMenuLabel1.Name = "MainMenuLabel1";
                    _mainMenuLabel1.LineNumber = 6;
                    _mainMenuLabel1.LinePosition = 8;
#endif
                    Delight.Label.FontColorProperty.SetDefault(_mainMenuLabel1, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Label.FontSizeProperty.SetDefault(_mainMenuLabel1, 40f);
                    Delight.Label.FontProperty.SetDefault(_mainMenuLabel1, Assets.TMP_FontAssets["AveriaSansLibre-Bold SDF"]);
                    Delight.Label.AutoSizeProperty.SetDefault(_mainMenuLabel1, Delight.AutoSize.Default);
                    Delight.Label.TextProperty.SetDefault(_mainMenuLabel1, "Main Menu");
                    Delight.Label.OffsetProperty.SetDefault(_mainMenuLabel1, new ElementMargin(new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(210f, ElementSizeUnit.Pixels)));
                }
                return _mainMenuLabel1;
            }
        }

        private static Template _mainMenuGroup1;
        public static Template MainMenuGroup1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuGroup1 == null || _mainMenuGroup1.CurrentVersion != Template.Version)
#else
                if (_mainMenuGroup1 == null)
#endif
                {
                    _mainMenuGroup1 = new Template(GroupTemplates.Group);
#if UNITY_EDITOR
                    _mainMenuGroup1.Name = "MainMenuGroup1";
                    _mainMenuGroup1.LineNumber = 7;
                    _mainMenuGroup1.LinePosition = 8;
#endif
                    Delight.Group.SpacingProperty.SetDefault(_mainMenuGroup1, new ElementSize(10f, ElementSizeUnit.Pixels));
                    Delight.Group.OffsetProperty.SetDefault(_mainMenuGroup1, new ElementMargin(new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(25f, ElementSizeUnit.Pixels), new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(0f, ElementSizeUnit.Pixels)));
                }
                return _mainMenuGroup1;
            }
        }

        private static Template _mainMenuButton1;
        public static Template MainMenuButton1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuButton1 == null || _mainMenuButton1.CurrentVersion != Template.Version)
#else
                if (_mainMenuButton1 == null)
#endif
                {
                    _mainMenuButton1 = new Template(ButtonTemplates.Button);
#if UNITY_EDITOR
                    _mainMenuButton1.Name = "MainMenuButton1";
                    _mainMenuButton1.LineNumber = 8;
                    _mainMenuButton1.LinePosition = 10;
#endif
                    Delight.Button.BackgroundSpriteProperty.SetDefault(_mainMenuButton1, Assets.Sprites["MainMenuDemoButton"]);
                    Delight.Button.BackgroundSpriteProperty.SetStateDefault("Pressed", _mainMenuButton1, Assets.Sprites["MainMenuDemoButtonPressed"]);
                    Delight.Button.BackgroundColorProperty.SetDefault(_mainMenuButton1, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Button.BackgroundColorProperty.SetStateDefault("Highlighted", _mainMenuButton1, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Button.BackgroundColorProperty.SetStateDefault("Pressed", _mainMenuButton1, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Button.WidthProperty.SetDefault(_mainMenuButton1, new ElementSize(218f, ElementSizeUnit.Pixels));
                    Delight.Button.HeightProperty.SetDefault(_mainMenuButton1, new ElementSize(117f, ElementSizeUnit.Pixels));
                    Delight.Button.TextOffsetProperty.SetDefault(_mainMenuButton1, new ElementMargin(new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(6f, ElementSizeUnit.Pixels)));
                    Delight.Button.LabelTemplateProperty.SetDefault(_mainMenuButton1, MainMenuButton1Label);
                }
                return _mainMenuButton1;
            }
        }

        private static Template _mainMenuButton1Label;
        public static Template MainMenuButton1Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuButton1Label == null || _mainMenuButton1Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuButton1Label == null)
#endif
                {
                    _mainMenuButton1Label = new Template(ButtonTemplates.ButtonLabel);
#if UNITY_EDITOR
                    _mainMenuButton1Label.Name = "MainMenuButton1Label";
                    _mainMenuButton1Label.LineNumber = 15;
                    _mainMenuButton1Label.LinePosition = 4;
#endif
                    Delight.Label.FontSizeProperty.SetDefault(_mainMenuButton1Label, 40f);
                    Delight.Label.FontProperty.SetDefault(_mainMenuButton1Label, Assets.TMP_FontAssets["AveriaSansLibre-Bold SDF"]);
                    Delight.Label.FontColorProperty.SetDefault(_mainMenuButton1Label, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Label.FontColorProperty.SetStateDefault("Highlighted", _mainMenuButton1Label, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Label.FontColorProperty.SetStateDefault("Pressed", _mainMenuButton1Label, new UnityEngine.Color(0.8f, 0.8f, 0.8f, 1f));
                    Delight.Label.TextProperty.SetDefault(_mainMenuButton1Label, "Play");
                }
                return _mainMenuButton1Label;
            }
        }

        private static Template _mainMenuButton2;
        public static Template MainMenuButton2
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuButton2 == null || _mainMenuButton2.CurrentVersion != Template.Version)
#else
                if (_mainMenuButton2 == null)
#endif
                {
                    _mainMenuButton2 = new Template(ButtonTemplates.Button);
#if UNITY_EDITOR
                    _mainMenuButton2.Name = "MainMenuButton2";
                    _mainMenuButton2.LineNumber = 9;
                    _mainMenuButton2.LinePosition = 10;
#endif
                    Delight.Button.BackgroundSpriteProperty.SetDefault(_mainMenuButton2, Assets.Sprites["MainMenuDemoButton"]);
                    Delight.Button.BackgroundSpriteProperty.SetStateDefault("Pressed", _mainMenuButton2, Assets.Sprites["MainMenuDemoButtonPressed"]);
                    Delight.Button.BackgroundColorProperty.SetDefault(_mainMenuButton2, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Button.BackgroundColorProperty.SetStateDefault("Highlighted", _mainMenuButton2, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Button.BackgroundColorProperty.SetStateDefault("Pressed", _mainMenuButton2, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Button.WidthProperty.SetDefault(_mainMenuButton2, new ElementSize(218f, ElementSizeUnit.Pixels));
                    Delight.Button.HeightProperty.SetDefault(_mainMenuButton2, new ElementSize(117f, ElementSizeUnit.Pixels));
                    Delight.Button.TextOffsetProperty.SetDefault(_mainMenuButton2, new ElementMargin(new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(6f, ElementSizeUnit.Pixels)));
                    Delight.Button.LabelTemplateProperty.SetDefault(_mainMenuButton2, MainMenuButton2Label);
                }
                return _mainMenuButton2;
            }
        }

        private static Template _mainMenuButton2Label;
        public static Template MainMenuButton2Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuButton2Label == null || _mainMenuButton2Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuButton2Label == null)
#endif
                {
                    _mainMenuButton2Label = new Template(ButtonTemplates.ButtonLabel);
#if UNITY_EDITOR
                    _mainMenuButton2Label.Name = "MainMenuButton2Label";
                    _mainMenuButton2Label.LineNumber = 15;
                    _mainMenuButton2Label.LinePosition = 4;
#endif
                    Delight.Label.FontSizeProperty.SetDefault(_mainMenuButton2Label, 40f);
                    Delight.Label.FontProperty.SetDefault(_mainMenuButton2Label, Assets.TMP_FontAssets["AveriaSansLibre-Bold SDF"]);
                    Delight.Label.FontColorProperty.SetDefault(_mainMenuButton2Label, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Label.FontColorProperty.SetStateDefault("Highlighted", _mainMenuButton2Label, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Label.FontColorProperty.SetStateDefault("Pressed", _mainMenuButton2Label, new UnityEngine.Color(0.8f, 0.8f, 0.8f, 1f));
                    Delight.Label.TextProperty.SetDefault(_mainMenuButton2Label, "Options");
                }
                return _mainMenuButton2Label;
            }
        }

        private static Template _mainMenuButton3;
        public static Template MainMenuButton3
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuButton3 == null || _mainMenuButton3.CurrentVersion != Template.Version)
#else
                if (_mainMenuButton3 == null)
#endif
                {
                    _mainMenuButton3 = new Template(ButtonTemplates.Button);
#if UNITY_EDITOR
                    _mainMenuButton3.Name = "MainMenuButton3";
                    _mainMenuButton3.LineNumber = 10;
                    _mainMenuButton3.LinePosition = 10;
#endif
                    Delight.Button.BackgroundSpriteProperty.SetDefault(_mainMenuButton3, Assets.Sprites["MainMenuDemoButton"]);
                    Delight.Button.BackgroundSpriteProperty.SetStateDefault("Pressed", _mainMenuButton3, Assets.Sprites["MainMenuDemoButtonPressed"]);
                    Delight.Button.BackgroundColorProperty.SetDefault(_mainMenuButton3, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Button.BackgroundColorProperty.SetStateDefault("Highlighted", _mainMenuButton3, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Button.BackgroundColorProperty.SetStateDefault("Pressed", _mainMenuButton3, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Button.WidthProperty.SetDefault(_mainMenuButton3, new ElementSize(218f, ElementSizeUnit.Pixels));
                    Delight.Button.HeightProperty.SetDefault(_mainMenuButton3, new ElementSize(117f, ElementSizeUnit.Pixels));
                    Delight.Button.TextOffsetProperty.SetDefault(_mainMenuButton3, new ElementMargin(new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(0f, ElementSizeUnit.Pixels), new ElementSize(6f, ElementSizeUnit.Pixels)));
                    Delight.Button.LabelTemplateProperty.SetDefault(_mainMenuButton3, MainMenuButton3Label);
                }
                return _mainMenuButton3;
            }
        }

        private static Template _mainMenuButton3Label;
        public static Template MainMenuButton3Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuButton3Label == null || _mainMenuButton3Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuButton3Label == null)
#endif
                {
                    _mainMenuButton3Label = new Template(ButtonTemplates.ButtonLabel);
#if UNITY_EDITOR
                    _mainMenuButton3Label.Name = "MainMenuButton3Label";
                    _mainMenuButton3Label.LineNumber = 15;
                    _mainMenuButton3Label.LinePosition = 4;
#endif
                    Delight.Label.FontSizeProperty.SetDefault(_mainMenuButton3Label, 40f);
                    Delight.Label.FontProperty.SetDefault(_mainMenuButton3Label, Assets.TMP_FontAssets["AveriaSansLibre-Bold SDF"]);
                    Delight.Label.FontColorProperty.SetDefault(_mainMenuButton3Label, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Label.FontColorProperty.SetStateDefault("Highlighted", _mainMenuButton3Label, new UnityEngine.Color(1f, 1f, 1f, 1f));
                    Delight.Label.FontColorProperty.SetStateDefault("Pressed", _mainMenuButton3Label, new UnityEngine.Color(0.8f, 0.8f, 0.8f, 1f));
                    Delight.Label.TextProperty.SetDefault(_mainMenuButton3Label, "Quit");
                }
                return _mainMenuButton3Label;
            }
        }

        private static Template _mainMenuLevelSelectWindow;
        public static Template MainMenuLevelSelectWindow
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindow == null || _mainMenuLevelSelectWindow.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindow == null)
#endif
                {
                    _mainMenuLevelSelectWindow = new Template(LevelSelectExampleTemplates.LevelSelectExample);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindow.Name = "MainMenuLevelSelectWindow";
                    _mainMenuLevelSelectWindow.LineNumber = 15;
                    _mainMenuLevelSelectWindow.LinePosition = 6;
#endif
                    Delight.LevelSelectExample.Image1TemplateProperty.SetDefault(_mainMenuLevelSelectWindow, MainMenuLevelSelectWindowImage1);
                    Delight.LevelSelectExample.Label1TemplateProperty.SetDefault(_mainMenuLevelSelectWindow, MainMenuLevelSelectWindowLabel1);
                    Delight.LevelSelectExample.List1TemplateProperty.SetDefault(_mainMenuLevelSelectWindow, MainMenuLevelSelectWindowList1);
                    Delight.LevelSelectExample.ListItem1TemplateProperty.SetDefault(_mainMenuLevelSelectWindow, MainMenuLevelSelectWindowListItem1);
                    Delight.LevelSelectExample.Label2TemplateProperty.SetDefault(_mainMenuLevelSelectWindow, MainMenuLevelSelectWindowLabel2);
                    Delight.LevelSelectExample.Image2TemplateProperty.SetDefault(_mainMenuLevelSelectWindow, MainMenuLevelSelectWindowImage2);
                    Delight.LevelSelectExample.NavigationButton1TemplateProperty.SetDefault(_mainMenuLevelSelectWindow, MainMenuLevelSelectWindowNavigationButton1);
                    Delight.LevelSelectExample.NavigationButton2TemplateProperty.SetDefault(_mainMenuLevelSelectWindow, MainMenuLevelSelectWindowNavigationButton2);
                    Delight.LevelSelectExample.Button1TemplateProperty.SetDefault(_mainMenuLevelSelectWindow, MainMenuLevelSelectWindowButton1);
                }
                return _mainMenuLevelSelectWindow;
            }
        }

        private static Template _mainMenuLevelSelectWindowImage1;
        public static Template MainMenuLevelSelectWindowImage1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowImage1 == null || _mainMenuLevelSelectWindowImage1.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowImage1 == null)
#endif
                {
                    _mainMenuLevelSelectWindowImage1 = new Template(LevelSelectExampleTemplates.LevelSelectExampleImage1);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowImage1.Name = "MainMenuLevelSelectWindowImage1";
                    _mainMenuLevelSelectWindowImage1.LineNumber = 3;
                    _mainMenuLevelSelectWindowImage1.LinePosition = 4;
#endif
                }
                return _mainMenuLevelSelectWindowImage1;
            }
        }

        private static Template _mainMenuLevelSelectWindowLabel1;
        public static Template MainMenuLevelSelectWindowLabel1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowLabel1 == null || _mainMenuLevelSelectWindowLabel1.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowLabel1 == null)
#endif
                {
                    _mainMenuLevelSelectWindowLabel1 = new Template(LevelSelectExampleTemplates.LevelSelectExampleLabel1);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowLabel1.Name = "MainMenuLevelSelectWindowLabel1";
                    _mainMenuLevelSelectWindowLabel1.LineNumber = 4;
                    _mainMenuLevelSelectWindowLabel1.LinePosition = 4;
#endif
                }
                return _mainMenuLevelSelectWindowLabel1;
            }
        }

        private static Template _mainMenuLevelSelectWindowList1;
        public static Template MainMenuLevelSelectWindowList1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowList1 == null || _mainMenuLevelSelectWindowList1.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowList1 == null)
#endif
                {
                    _mainMenuLevelSelectWindowList1 = new Template(LevelSelectExampleTemplates.LevelSelectExampleList1);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowList1.Name = "MainMenuLevelSelectWindowList1";
                    _mainMenuLevelSelectWindowList1.LineNumber = 5;
                    _mainMenuLevelSelectWindowList1.LinePosition = 4;
#endif
                    Delight.List.ScrollableRegionTemplateProperty.SetDefault(_mainMenuLevelSelectWindowList1, MainMenuLevelSelectWindowList1ScrollableRegion);
                }
                return _mainMenuLevelSelectWindowList1;
            }
        }

        private static Template _mainMenuLevelSelectWindowList1ScrollableRegion;
        public static Template MainMenuLevelSelectWindowList1ScrollableRegion
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowList1ScrollableRegion == null || _mainMenuLevelSelectWindowList1ScrollableRegion.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowList1ScrollableRegion == null)
#endif
                {
                    _mainMenuLevelSelectWindowList1ScrollableRegion = new Template(LevelSelectExampleTemplates.LevelSelectExampleList1ScrollableRegion);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowList1ScrollableRegion.Name = "MainMenuLevelSelectWindowList1ScrollableRegion";
                    _mainMenuLevelSelectWindowList1ScrollableRegion.LineNumber = 29;
                    _mainMenuLevelSelectWindowList1ScrollableRegion.LinePosition = 4;
#endif
                    Delight.ScrollableRegion.ContentRegionTemplateProperty.SetDefault(_mainMenuLevelSelectWindowList1ScrollableRegion, MainMenuLevelSelectWindowList1ScrollableRegionContentRegion);
                    Delight.ScrollableRegion.HorizontalScrollbarTemplateProperty.SetDefault(_mainMenuLevelSelectWindowList1ScrollableRegion, MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar);
                    Delight.ScrollableRegion.VerticalScrollbarTemplateProperty.SetDefault(_mainMenuLevelSelectWindowList1ScrollableRegion, MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar);
                }
                return _mainMenuLevelSelectWindowList1ScrollableRegion;
            }
        }

        private static Template _mainMenuLevelSelectWindowList1ScrollableRegionContentRegion;
        public static Template MainMenuLevelSelectWindowList1ScrollableRegionContentRegion
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowList1ScrollableRegionContentRegion == null || _mainMenuLevelSelectWindowList1ScrollableRegionContentRegion.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowList1ScrollableRegionContentRegion == null)
#endif
                {
                    _mainMenuLevelSelectWindowList1ScrollableRegionContentRegion = new Template(LevelSelectExampleTemplates.LevelSelectExampleList1ScrollableRegionContentRegion);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowList1ScrollableRegionContentRegion.Name = "MainMenuLevelSelectWindowList1ScrollableRegionContentRegion";
                    _mainMenuLevelSelectWindowList1ScrollableRegionContentRegion.LineNumber = 24;
                    _mainMenuLevelSelectWindowList1ScrollableRegionContentRegion.LinePosition = 4;
#endif
                }
                return _mainMenuLevelSelectWindowList1ScrollableRegionContentRegion;
            }
        }

        private static Template _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar;
        public static Template MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar == null || _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar == null)
#endif
                {
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar = new Template(LevelSelectExampleTemplates.LevelSelectExampleList1ScrollableRegionHorizontalScrollbar);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar.Name = "MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar";
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar.LineNumber = 26;
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar.LinePosition = 4;
#endif
                    Delight.Scrollbar.BarTemplateProperty.SetDefault(_mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar, MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar);
                    Delight.Scrollbar.HandleTemplateProperty.SetDefault(_mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar, MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle);
                }
                return _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar;
            }
        }

        private static Template _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar;
        public static Template MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar == null || _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar == null)
#endif
                {
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar = new Template(LevelSelectExampleTemplates.LevelSelectExampleList1ScrollableRegionHorizontalScrollbarBar);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar.Name = "MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar";
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar.LineNumber = 7;
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar.LinePosition = 4;
#endif
                }
                return _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar;
            }
        }

        private static Template _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle;
        public static Template MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle == null || _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle == null)
#endif
                {
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle = new Template(LevelSelectExampleTemplates.LevelSelectExampleList1ScrollableRegionHorizontalScrollbarHandle);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle.Name = "MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle";
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle.LineNumber = 8;
                    _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle.LinePosition = 6;
#endif
                }
                return _mainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle;
            }
        }

        private static Template _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar;
        public static Template MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar == null || _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar == null)
#endif
                {
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar = new Template(LevelSelectExampleTemplates.LevelSelectExampleList1ScrollableRegionVerticalScrollbar);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar.Name = "MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar";
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar.LineNumber = 27;
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar.LinePosition = 4;
#endif
                    Delight.Scrollbar.BarTemplateProperty.SetDefault(_mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar, MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar);
                    Delight.Scrollbar.HandleTemplateProperty.SetDefault(_mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar, MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle);
                }
                return _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar;
            }
        }

        private static Template _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar;
        public static Template MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar == null || _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar == null)
#endif
                {
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar = new Template(LevelSelectExampleTemplates.LevelSelectExampleList1ScrollableRegionVerticalScrollbarBar);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar.Name = "MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar";
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar.LineNumber = 7;
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar.LinePosition = 4;
#endif
                }
                return _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar;
            }
        }

        private static Template _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle;
        public static Template MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle == null || _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle == null)
#endif
                {
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle = new Template(LevelSelectExampleTemplates.LevelSelectExampleList1ScrollableRegionVerticalScrollbarHandle);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle.Name = "MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle";
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle.LineNumber = 8;
                    _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle.LinePosition = 6;
#endif
                }
                return _mainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle;
            }
        }

        private static Template _mainMenuLevelSelectWindowListItem1;
        public static Template MainMenuLevelSelectWindowListItem1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowListItem1 == null || _mainMenuLevelSelectWindowListItem1.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowListItem1 == null)
#endif
                {
                    _mainMenuLevelSelectWindowListItem1 = new Template(LevelSelectExampleTemplates.LevelSelectExampleListItem1);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowListItem1.Name = "MainMenuLevelSelectWindowListItem1";
                    _mainMenuLevelSelectWindowListItem1.LineNumber = 10;
                    _mainMenuLevelSelectWindowListItem1.LinePosition = 6;
#endif
                }
                return _mainMenuLevelSelectWindowListItem1;
            }
        }

        private static Template _mainMenuLevelSelectWindowLabel2;
        public static Template MainMenuLevelSelectWindowLabel2
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowLabel2 == null || _mainMenuLevelSelectWindowLabel2.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowLabel2 == null)
#endif
                {
                    _mainMenuLevelSelectWindowLabel2 = new Template(LevelSelectExampleTemplates.LevelSelectExampleLabel2);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowLabel2.Name = "MainMenuLevelSelectWindowLabel2";
                    _mainMenuLevelSelectWindowLabel2.LineNumber = 11;
                    _mainMenuLevelSelectWindowLabel2.LinePosition = 8;
#endif
                }
                return _mainMenuLevelSelectWindowLabel2;
            }
        }

        private static Template _mainMenuLevelSelectWindowImage2;
        public static Template MainMenuLevelSelectWindowImage2
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowImage2 == null || _mainMenuLevelSelectWindowImage2.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowImage2 == null)
#endif
                {
                    _mainMenuLevelSelectWindowImage2 = new Template(LevelSelectExampleTemplates.LevelSelectExampleImage2);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowImage2.Name = "MainMenuLevelSelectWindowImage2";
                    _mainMenuLevelSelectWindowImage2.LineNumber = 13;
                    _mainMenuLevelSelectWindowImage2.LinePosition = 8;
#endif
                }
                return _mainMenuLevelSelectWindowImage2;
            }
        }

        private static Template _mainMenuLevelSelectWindowNavigationButton1;
        public static Template MainMenuLevelSelectWindowNavigationButton1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowNavigationButton1 == null || _mainMenuLevelSelectWindowNavigationButton1.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowNavigationButton1 == null)
#endif
                {
                    _mainMenuLevelSelectWindowNavigationButton1 = new Template(LevelSelectExampleTemplates.LevelSelectExampleNavigationButton1);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowNavigationButton1.Name = "MainMenuLevelSelectWindowNavigationButton1";
                    _mainMenuLevelSelectWindowNavigationButton1.LineNumber = 15;
                    _mainMenuLevelSelectWindowNavigationButton1.LinePosition = 6;
#endif
                    Delight.NavigationButton.LabelTemplateProperty.SetDefault(_mainMenuLevelSelectWindowNavigationButton1, MainMenuLevelSelectWindowNavigationButton1Label);
                }
                return _mainMenuLevelSelectWindowNavigationButton1;
            }
        }

        private static Template _mainMenuLevelSelectWindowNavigationButton1Label;
        public static Template MainMenuLevelSelectWindowNavigationButton1Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowNavigationButton1Label == null || _mainMenuLevelSelectWindowNavigationButton1Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowNavigationButton1Label == null)
#endif
                {
                    _mainMenuLevelSelectWindowNavigationButton1Label = new Template(LevelSelectExampleTemplates.LevelSelectExampleNavigationButton1Label);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowNavigationButton1Label.Name = "MainMenuLevelSelectWindowNavigationButton1Label";
                    _mainMenuLevelSelectWindowNavigationButton1Label.LineNumber = 15;
                    _mainMenuLevelSelectWindowNavigationButton1Label.LinePosition = 4;
#endif
                }
                return _mainMenuLevelSelectWindowNavigationButton1Label;
            }
        }

        private static Template _mainMenuLevelSelectWindowNavigationButton2;
        public static Template MainMenuLevelSelectWindowNavigationButton2
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowNavigationButton2 == null || _mainMenuLevelSelectWindowNavigationButton2.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowNavigationButton2 == null)
#endif
                {
                    _mainMenuLevelSelectWindowNavigationButton2 = new Template(LevelSelectExampleTemplates.LevelSelectExampleNavigationButton2);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowNavigationButton2.Name = "MainMenuLevelSelectWindowNavigationButton2";
                    _mainMenuLevelSelectWindowNavigationButton2.LineNumber = 18;
                    _mainMenuLevelSelectWindowNavigationButton2.LinePosition = 6;
#endif
                    Delight.NavigationButton.LabelTemplateProperty.SetDefault(_mainMenuLevelSelectWindowNavigationButton2, MainMenuLevelSelectWindowNavigationButton2Label);
                }
                return _mainMenuLevelSelectWindowNavigationButton2;
            }
        }

        private static Template _mainMenuLevelSelectWindowNavigationButton2Label;
        public static Template MainMenuLevelSelectWindowNavigationButton2Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowNavigationButton2Label == null || _mainMenuLevelSelectWindowNavigationButton2Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowNavigationButton2Label == null)
#endif
                {
                    _mainMenuLevelSelectWindowNavigationButton2Label = new Template(LevelSelectExampleTemplates.LevelSelectExampleNavigationButton2Label);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowNavigationButton2Label.Name = "MainMenuLevelSelectWindowNavigationButton2Label";
                    _mainMenuLevelSelectWindowNavigationButton2Label.LineNumber = 15;
                    _mainMenuLevelSelectWindowNavigationButton2Label.LinePosition = 4;
#endif
                }
                return _mainMenuLevelSelectWindowNavigationButton2Label;
            }
        }

        private static Template _mainMenuLevelSelectWindowButton1;
        public static Template MainMenuLevelSelectWindowButton1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowButton1 == null || _mainMenuLevelSelectWindowButton1.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowButton1 == null)
#endif
                {
                    _mainMenuLevelSelectWindowButton1 = new Template(LevelSelectExampleTemplates.LevelSelectExampleButton1);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowButton1.Name = "MainMenuLevelSelectWindowButton1";
                    _mainMenuLevelSelectWindowButton1.LineNumber = 22;
                    _mainMenuLevelSelectWindowButton1.LinePosition = 4;
#endif
                    Delight.Button.LabelTemplateProperty.SetDefault(_mainMenuLevelSelectWindowButton1, MainMenuLevelSelectWindowButton1Label);
                }
                return _mainMenuLevelSelectWindowButton1;
            }
        }

        private static Template _mainMenuLevelSelectWindowButton1Label;
        public static Template MainMenuLevelSelectWindowButton1Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuLevelSelectWindowButton1Label == null || _mainMenuLevelSelectWindowButton1Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuLevelSelectWindowButton1Label == null)
#endif
                {
                    _mainMenuLevelSelectWindowButton1Label = new Template(LevelSelectExampleTemplates.LevelSelectExampleButton1Label);
#if UNITY_EDITOR
                    _mainMenuLevelSelectWindowButton1Label.Name = "MainMenuLevelSelectWindowButton1Label";
                    _mainMenuLevelSelectWindowButton1Label.LineNumber = 15;
                    _mainMenuLevelSelectWindowButton1Label.LinePosition = 4;
#endif
                }
                return _mainMenuLevelSelectWindowButton1Label;
            }
        }

        #endregion
    }

    #endregion
}
