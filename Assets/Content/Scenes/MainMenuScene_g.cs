// Internal view logic generated from "MainMenuScene.xml"
#region Using Statements
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
#endregion

namespace Delight
{
    public partial class MainMenuScene : UIView
    {
        #region Constructors

        public MainMenuScene(View parent, View layoutParent = null, string id = null, Template template = null, bool deferInitialization = false) :
            base(parent, layoutParent, id, template ?? MainMenuSceneTemplates.Default, deferInitialization)
        {
            if (deferInitialization)
                return;

            // constructing MainMenu (MainMenu1)
            MainMenu1 = new MainMenu(this, this, "MainMenu1", MainMenu1Template);
            this.AfterInitializeInternal();
        }

        public MainMenuScene() : this(null)
        {
        }

        static MainMenuScene()
        {
            var dependencyProperties = new List<DependencyProperty>();
            DependencyProperties.Add(MainMenuSceneTemplates.Default, dependencyProperties);

            dependencyProperties.Add(MainMenu1Property);
            dependencyProperties.Add(MainMenu1TemplateProperty);
        }

        #endregion

        #region Properties

        public readonly static DependencyProperty<MainMenu> MainMenu1Property = new DependencyProperty<MainMenu>("MainMenu1");
        public MainMenu MainMenu1
        {
            get { return MainMenu1Property.GetValue(this); }
            set { MainMenu1Property.SetValue(this, value); }
        }

        public readonly static DependencyProperty<Template> MainMenu1TemplateProperty = new DependencyProperty<Template>("MainMenu1Template");
        public Template MainMenu1Template
        {
            get { return MainMenu1TemplateProperty.GetValue(this); }
            set { MainMenu1TemplateProperty.SetValue(this, value); }
        }

        #endregion
    }

    #region Data Templates

    public static class MainMenuSceneTemplates
    {
        #region Properties

        public static Template Default
        {
            get
            {
                return MainMenuScene;
            }
        }

        private static Template _mainMenuScene;
        public static Template MainMenuScene
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuScene == null || _mainMenuScene.CurrentVersion != Template.Version)
#else
                if (_mainMenuScene == null)
#endif
                {
                    _mainMenuScene = new Template(UIViewTemplates.UIView);
#if UNITY_EDITOR
                    _mainMenuScene.Name = "MainMenuScene";
                    _mainMenuScene.LineNumber = 0;
                    _mainMenuScene.LinePosition = 0;
#endif
                    Delight.MainMenuScene.MainMenu1TemplateProperty.SetDefault(_mainMenuScene, MainMenuSceneMainMenu1);
                }
                return _mainMenuScene;
            }
        }

        private static Template _mainMenuSceneMainMenu1;
        public static Template MainMenuSceneMainMenu1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1 == null || _mainMenuSceneMainMenu1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1 = new Template(MainMenuTemplates.MainMenu);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1.Name = "MainMenuSceneMainMenu1";
                    _mainMenuSceneMainMenu1.LineNumber = 2;
                    _mainMenuSceneMainMenu1.LinePosition = 4;
#endif
                    Delight.MainMenu.SubmenuSwitcherTemplateProperty.SetDefault(_mainMenuSceneMainMenu1, MainMenuSceneMainMenu1SubmenuSwitcher);
                    Delight.MainMenu.MainMenuWindowTemplateProperty.SetDefault(_mainMenuSceneMainMenu1, MainMenuSceneMainMenu1MainMenuWindow);
                    Delight.MainMenu.Image1TemplateProperty.SetDefault(_mainMenuSceneMainMenu1, MainMenuSceneMainMenu1Image1);
                    Delight.MainMenu.Label1TemplateProperty.SetDefault(_mainMenuSceneMainMenu1, MainMenuSceneMainMenu1Label1);
                    Delight.MainMenu.Group1TemplateProperty.SetDefault(_mainMenuSceneMainMenu1, MainMenuSceneMainMenu1Group1);
                    Delight.MainMenu.Button1TemplateProperty.SetDefault(_mainMenuSceneMainMenu1, MainMenuSceneMainMenu1Button1);
                    Delight.MainMenu.Button2TemplateProperty.SetDefault(_mainMenuSceneMainMenu1, MainMenuSceneMainMenu1Button2);
                    Delight.MainMenu.Button3TemplateProperty.SetDefault(_mainMenuSceneMainMenu1, MainMenuSceneMainMenu1Button3);
                    Delight.MainMenu.LevelSelectWindowTemplateProperty.SetDefault(_mainMenuSceneMainMenu1, MainMenuSceneMainMenu1LevelSelectWindow);
                }
                return _mainMenuSceneMainMenu1;
            }
        }

        private static Template _mainMenuSceneMainMenu1SubmenuSwitcher;
        public static Template MainMenuSceneMainMenu1SubmenuSwitcher
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1SubmenuSwitcher == null || _mainMenuSceneMainMenu1SubmenuSwitcher.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1SubmenuSwitcher == null)
#endif
                {
                    _mainMenuSceneMainMenu1SubmenuSwitcher = new Template(MainMenuTemplates.MainMenuSubmenuSwitcher);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1SubmenuSwitcher.Name = "MainMenuSceneMainMenu1SubmenuSwitcher";
                    _mainMenuSceneMainMenu1SubmenuSwitcher.LineNumber = 2;
                    _mainMenuSceneMainMenu1SubmenuSwitcher.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1SubmenuSwitcher;
            }
        }

        private static Template _mainMenuSceneMainMenu1MainMenuWindow;
        public static Template MainMenuSceneMainMenu1MainMenuWindow
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1MainMenuWindow == null || _mainMenuSceneMainMenu1MainMenuWindow.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1MainMenuWindow == null)
#endif
                {
                    _mainMenuSceneMainMenu1MainMenuWindow = new Template(MainMenuTemplates.MainMenuMainMenuWindow);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1MainMenuWindow.Name = "MainMenuSceneMainMenu1MainMenuWindow";
                    _mainMenuSceneMainMenu1MainMenuWindow.LineNumber = 4;
                    _mainMenuSceneMainMenu1MainMenuWindow.LinePosition = 6;
#endif
                }
                return _mainMenuSceneMainMenu1MainMenuWindow;
            }
        }

        private static Template _mainMenuSceneMainMenu1Image1;
        public static Template MainMenuSceneMainMenu1Image1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1Image1 == null || _mainMenuSceneMainMenu1Image1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1Image1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1Image1 = new Template(MainMenuTemplates.MainMenuImage1);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1Image1.Name = "MainMenuSceneMainMenu1Image1";
                    _mainMenuSceneMainMenu1Image1.LineNumber = 5;
                    _mainMenuSceneMainMenu1Image1.LinePosition = 8;
#endif
                }
                return _mainMenuSceneMainMenu1Image1;
            }
        }

        private static Template _mainMenuSceneMainMenu1Label1;
        public static Template MainMenuSceneMainMenu1Label1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1Label1 == null || _mainMenuSceneMainMenu1Label1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1Label1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1Label1 = new Template(MainMenuTemplates.MainMenuLabel1);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1Label1.Name = "MainMenuSceneMainMenu1Label1";
                    _mainMenuSceneMainMenu1Label1.LineNumber = 6;
                    _mainMenuSceneMainMenu1Label1.LinePosition = 8;
#endif
                }
                return _mainMenuSceneMainMenu1Label1;
            }
        }

        private static Template _mainMenuSceneMainMenu1Group1;
        public static Template MainMenuSceneMainMenu1Group1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1Group1 == null || _mainMenuSceneMainMenu1Group1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1Group1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1Group1 = new Template(MainMenuTemplates.MainMenuGroup1);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1Group1.Name = "MainMenuSceneMainMenu1Group1";
                    _mainMenuSceneMainMenu1Group1.LineNumber = 7;
                    _mainMenuSceneMainMenu1Group1.LinePosition = 8;
#endif
                }
                return _mainMenuSceneMainMenu1Group1;
            }
        }

        private static Template _mainMenuSceneMainMenu1Button1;
        public static Template MainMenuSceneMainMenu1Button1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1Button1 == null || _mainMenuSceneMainMenu1Button1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1Button1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1Button1 = new Template(MainMenuTemplates.MainMenuButton1);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1Button1.Name = "MainMenuSceneMainMenu1Button1";
                    _mainMenuSceneMainMenu1Button1.LineNumber = 8;
                    _mainMenuSceneMainMenu1Button1.LinePosition = 10;
#endif
                    Delight.Button.LabelTemplateProperty.SetDefault(_mainMenuSceneMainMenu1Button1, MainMenuSceneMainMenu1Button1Label);
                }
                return _mainMenuSceneMainMenu1Button1;
            }
        }

        private static Template _mainMenuSceneMainMenu1Button1Label;
        public static Template MainMenuSceneMainMenu1Button1Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1Button1Label == null || _mainMenuSceneMainMenu1Button1Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1Button1Label == null)
#endif
                {
                    _mainMenuSceneMainMenu1Button1Label = new Template(MainMenuTemplates.MainMenuButton1Label);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1Button1Label.Name = "MainMenuSceneMainMenu1Button1Label";
                    _mainMenuSceneMainMenu1Button1Label.LineNumber = 15;
                    _mainMenuSceneMainMenu1Button1Label.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1Button1Label;
            }
        }

        private static Template _mainMenuSceneMainMenu1Button2;
        public static Template MainMenuSceneMainMenu1Button2
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1Button2 == null || _mainMenuSceneMainMenu1Button2.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1Button2 == null)
#endif
                {
                    _mainMenuSceneMainMenu1Button2 = new Template(MainMenuTemplates.MainMenuButton2);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1Button2.Name = "MainMenuSceneMainMenu1Button2";
                    _mainMenuSceneMainMenu1Button2.LineNumber = 9;
                    _mainMenuSceneMainMenu1Button2.LinePosition = 10;
#endif
                    Delight.Button.LabelTemplateProperty.SetDefault(_mainMenuSceneMainMenu1Button2, MainMenuSceneMainMenu1Button2Label);
                }
                return _mainMenuSceneMainMenu1Button2;
            }
        }

        private static Template _mainMenuSceneMainMenu1Button2Label;
        public static Template MainMenuSceneMainMenu1Button2Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1Button2Label == null || _mainMenuSceneMainMenu1Button2Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1Button2Label == null)
#endif
                {
                    _mainMenuSceneMainMenu1Button2Label = new Template(MainMenuTemplates.MainMenuButton2Label);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1Button2Label.Name = "MainMenuSceneMainMenu1Button2Label";
                    _mainMenuSceneMainMenu1Button2Label.LineNumber = 15;
                    _mainMenuSceneMainMenu1Button2Label.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1Button2Label;
            }
        }

        private static Template _mainMenuSceneMainMenu1Button3;
        public static Template MainMenuSceneMainMenu1Button3
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1Button3 == null || _mainMenuSceneMainMenu1Button3.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1Button3 == null)
#endif
                {
                    _mainMenuSceneMainMenu1Button3 = new Template(MainMenuTemplates.MainMenuButton3);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1Button3.Name = "MainMenuSceneMainMenu1Button3";
                    _mainMenuSceneMainMenu1Button3.LineNumber = 10;
                    _mainMenuSceneMainMenu1Button3.LinePosition = 10;
#endif
                    Delight.Button.LabelTemplateProperty.SetDefault(_mainMenuSceneMainMenu1Button3, MainMenuSceneMainMenu1Button3Label);
                }
                return _mainMenuSceneMainMenu1Button3;
            }
        }

        private static Template _mainMenuSceneMainMenu1Button3Label;
        public static Template MainMenuSceneMainMenu1Button3Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1Button3Label == null || _mainMenuSceneMainMenu1Button3Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1Button3Label == null)
#endif
                {
                    _mainMenuSceneMainMenu1Button3Label = new Template(MainMenuTemplates.MainMenuButton3Label);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1Button3Label.Name = "MainMenuSceneMainMenu1Button3Label";
                    _mainMenuSceneMainMenu1Button3Label.LineNumber = 15;
                    _mainMenuSceneMainMenu1Button3Label.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1Button3Label;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindow;
        public static Template MainMenuSceneMainMenu1LevelSelectWindow
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindow == null || _mainMenuSceneMainMenu1LevelSelectWindow.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindow == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindow = new Template(MainMenuTemplates.MainMenuLevelSelectWindow);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindow.Name = "MainMenuSceneMainMenu1LevelSelectWindow";
                    _mainMenuSceneMainMenu1LevelSelectWindow.LineNumber = 15;
                    _mainMenuSceneMainMenu1LevelSelectWindow.LinePosition = 6;
#endif
                    Delight.LevelSelectExample.Image1TemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindow, MainMenuSceneMainMenu1LevelSelectWindowImage1);
                    Delight.LevelSelectExample.Label1TemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindow, MainMenuSceneMainMenu1LevelSelectWindowLabel1);
                    Delight.LevelSelectExample.List1TemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindow, MainMenuSceneMainMenu1LevelSelectWindowList1);
                    Delight.LevelSelectExample.ListItem1TemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindow, MainMenuSceneMainMenu1LevelSelectWindowListItem1);
                    Delight.LevelSelectExample.Label2TemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindow, MainMenuSceneMainMenu1LevelSelectWindowLabel2);
                    Delight.LevelSelectExample.Image2TemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindow, MainMenuSceneMainMenu1LevelSelectWindowImage2);
                    Delight.LevelSelectExample.NavigationButton1TemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindow, MainMenuSceneMainMenu1LevelSelectWindowNavigationButton1);
                    Delight.LevelSelectExample.NavigationButton2TemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindow, MainMenuSceneMainMenu1LevelSelectWindowNavigationButton2);
                    Delight.LevelSelectExample.Button1TemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindow, MainMenuSceneMainMenu1LevelSelectWindowButton1);
                }
                return _mainMenuSceneMainMenu1LevelSelectWindow;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowImage1;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowImage1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowImage1 == null || _mainMenuSceneMainMenu1LevelSelectWindowImage1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowImage1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowImage1 = new Template(MainMenuTemplates.MainMenuLevelSelectWindowImage1);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowImage1.Name = "MainMenuSceneMainMenu1LevelSelectWindowImage1";
                    _mainMenuSceneMainMenu1LevelSelectWindowImage1.LineNumber = 3;
                    _mainMenuSceneMainMenu1LevelSelectWindowImage1.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowImage1;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowLabel1;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowLabel1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowLabel1 == null || _mainMenuSceneMainMenu1LevelSelectWindowLabel1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowLabel1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowLabel1 = new Template(MainMenuTemplates.MainMenuLevelSelectWindowLabel1);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowLabel1.Name = "MainMenuSceneMainMenu1LevelSelectWindowLabel1";
                    _mainMenuSceneMainMenu1LevelSelectWindowLabel1.LineNumber = 4;
                    _mainMenuSceneMainMenu1LevelSelectWindowLabel1.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowLabel1;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowList1;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowList1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1 == null || _mainMenuSceneMainMenu1LevelSelectWindowList1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowList1 = new Template(MainMenuTemplates.MainMenuLevelSelectWindowList1);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowList1.Name = "MainMenuSceneMainMenu1LevelSelectWindowList1";
                    _mainMenuSceneMainMenu1LevelSelectWindowList1.LineNumber = 5;
                    _mainMenuSceneMainMenu1LevelSelectWindowList1.LinePosition = 4;
#endif
                    Delight.List.ScrollableRegionTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowList1, MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion);
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowList1;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion == null || _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion = new Template(MainMenuTemplates.MainMenuLevelSelectWindowList1ScrollableRegion);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion.Name = "MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion";
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion.LineNumber = 29;
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion.LinePosition = 4;
#endif
                    Delight.ScrollableRegion.ContentRegionTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion, MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion);
                    Delight.ScrollableRegion.HorizontalScrollbarTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion, MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar);
                    Delight.ScrollableRegion.VerticalScrollbarTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion, MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar);
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegion;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion == null || _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion = new Template(MainMenuTemplates.MainMenuLevelSelectWindowList1ScrollableRegionContentRegion);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion.Name = "MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion";
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion.LineNumber = 24;
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionContentRegion;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar == null || _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar = new Template(MainMenuTemplates.MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbar);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar.Name = "MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar";
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar.LineNumber = 26;
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar.LinePosition = 4;
#endif
                    Delight.Scrollbar.BarTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar, MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar);
                    Delight.Scrollbar.HandleTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar, MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle);
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbar;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar == null || _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar = new Template(MainMenuTemplates.MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar.Name = "MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar";
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar.LineNumber = 7;
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarBar;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle == null || _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle = new Template(MainMenuTemplates.MainMenuLevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle.Name = "MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle";
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle.LineNumber = 8;
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle.LinePosition = 6;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionHorizontalScrollbarHandle;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar == null || _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar = new Template(MainMenuTemplates.MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbar);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar.Name = "MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar";
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar.LineNumber = 27;
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar.LinePosition = 4;
#endif
                    Delight.Scrollbar.BarTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar, MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar);
                    Delight.Scrollbar.HandleTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar, MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle);
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbar;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar == null || _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar = new Template(MainMenuTemplates.MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarBar);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar.Name = "MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar";
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar.LineNumber = 7;
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarBar;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle == null || _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle = new Template(MainMenuTemplates.MainMenuLevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle.Name = "MainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle";
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle.LineNumber = 8;
                    _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle.LinePosition = 6;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowList1ScrollableRegionVerticalScrollbarHandle;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowListItem1;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowListItem1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowListItem1 == null || _mainMenuSceneMainMenu1LevelSelectWindowListItem1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowListItem1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowListItem1 = new Template(MainMenuTemplates.MainMenuLevelSelectWindowListItem1);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowListItem1.Name = "MainMenuSceneMainMenu1LevelSelectWindowListItem1";
                    _mainMenuSceneMainMenu1LevelSelectWindowListItem1.LineNumber = 10;
                    _mainMenuSceneMainMenu1LevelSelectWindowListItem1.LinePosition = 6;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowListItem1;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowLabel2;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowLabel2
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowLabel2 == null || _mainMenuSceneMainMenu1LevelSelectWindowLabel2.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowLabel2 == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowLabel2 = new Template(MainMenuTemplates.MainMenuLevelSelectWindowLabel2);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowLabel2.Name = "MainMenuSceneMainMenu1LevelSelectWindowLabel2";
                    _mainMenuSceneMainMenu1LevelSelectWindowLabel2.LineNumber = 11;
                    _mainMenuSceneMainMenu1LevelSelectWindowLabel2.LinePosition = 8;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowLabel2;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowImage2;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowImage2
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowImage2 == null || _mainMenuSceneMainMenu1LevelSelectWindowImage2.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowImage2 == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowImage2 = new Template(MainMenuTemplates.MainMenuLevelSelectWindowImage2);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowImage2.Name = "MainMenuSceneMainMenu1LevelSelectWindowImage2";
                    _mainMenuSceneMainMenu1LevelSelectWindowImage2.LineNumber = 13;
                    _mainMenuSceneMainMenu1LevelSelectWindowImage2.LinePosition = 8;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowImage2;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowNavigationButton1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1 == null || _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1 = new Template(MainMenuTemplates.MainMenuLevelSelectWindowNavigationButton1);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1.Name = "MainMenuSceneMainMenu1LevelSelectWindowNavigationButton1";
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1.LineNumber = 15;
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1.LinePosition = 6;
#endif
                    Delight.NavigationButton.LabelTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1, MainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label);
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label == null || _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label = new Template(MainMenuTemplates.MainMenuLevelSelectWindowNavigationButton1Label);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label.Name = "MainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label";
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label.LineNumber = 15;
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton1Label;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowNavigationButton2
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2 == null || _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2 == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2 = new Template(MainMenuTemplates.MainMenuLevelSelectWindowNavigationButton2);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2.Name = "MainMenuSceneMainMenu1LevelSelectWindowNavigationButton2";
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2.LineNumber = 18;
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2.LinePosition = 6;
#endif
                    Delight.NavigationButton.LabelTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2, MainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label);
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label == null || _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label = new Template(MainMenuTemplates.MainMenuLevelSelectWindowNavigationButton2Label);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label.Name = "MainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label";
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label.LineNumber = 15;
                    _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowNavigationButton2Label;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowButton1;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowButton1
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowButton1 == null || _mainMenuSceneMainMenu1LevelSelectWindowButton1.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowButton1 == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowButton1 = new Template(MainMenuTemplates.MainMenuLevelSelectWindowButton1);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowButton1.Name = "MainMenuSceneMainMenu1LevelSelectWindowButton1";
                    _mainMenuSceneMainMenu1LevelSelectWindowButton1.LineNumber = 22;
                    _mainMenuSceneMainMenu1LevelSelectWindowButton1.LinePosition = 4;
#endif
                    Delight.Button.LabelTemplateProperty.SetDefault(_mainMenuSceneMainMenu1LevelSelectWindowButton1, MainMenuSceneMainMenu1LevelSelectWindowButton1Label);
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowButton1;
            }
        }

        private static Template _mainMenuSceneMainMenu1LevelSelectWindowButton1Label;
        public static Template MainMenuSceneMainMenu1LevelSelectWindowButton1Label
        {
            get
            {
#if UNITY_EDITOR
                if (_mainMenuSceneMainMenu1LevelSelectWindowButton1Label == null || _mainMenuSceneMainMenu1LevelSelectWindowButton1Label.CurrentVersion != Template.Version)
#else
                if (_mainMenuSceneMainMenu1LevelSelectWindowButton1Label == null)
#endif
                {
                    _mainMenuSceneMainMenu1LevelSelectWindowButton1Label = new Template(MainMenuTemplates.MainMenuLevelSelectWindowButton1Label);
#if UNITY_EDITOR
                    _mainMenuSceneMainMenu1LevelSelectWindowButton1Label.Name = "MainMenuSceneMainMenu1LevelSelectWindowButton1Label";
                    _mainMenuSceneMainMenu1LevelSelectWindowButton1Label.LineNumber = 15;
                    _mainMenuSceneMainMenu1LevelSelectWindowButton1Label.LinePosition = 4;
#endif
                }
                return _mainMenuSceneMainMenu1LevelSelectWindowButton1Label;
            }
        }

        #endregion
    }

    #endregion
}
