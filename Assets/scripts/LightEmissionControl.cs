﻿using System.Collections.Generic;
using UnityEngine;

public class LightEmissionControl : MonoBehaviour
{
    public List<GameObject> sceneObjs;
    public Color lightColor;
    public Material lightMaterial;
    public Shader glowShader;

    // Start is called before the first frame update
    void Start()
    {
        glowShader = Shader.Find("Standard");
        if (glowShader == null)
        {
            Debug.LogErrorFormat("{0} - {1} shader was not found.", GetType(), glowShader.name);
        }
        else
        {
            //attach the standard Unity shader to the material created for this project
            lightMaterial = new Material(glowShader);
            if (lightColor == null)
            {
                lightColor = Color.magenta;
                Debug.LogWarningFormat("{0} - Defaulting color to {1}",GetType(), lightColor);
            }
            if (lightMaterial != null)
            {
                lightMaterial.color = lightColor;
                Debug.LogFormat("{0} - material color is {1}",GetType(),lightMaterial.color);
            }
            else
            {
                Debug.LogErrorFormat("{0} - There was an error with getting the shader.", GetType());
            }
        }
        SetAllObjsColor();
    }

    public void GreenClick()
    {
        lightColor = Color.green;
        lightMaterial.color = lightColor;
    }

    public void YellowClick()
    {
        lightColor = Color.yellow;
        lightMaterial.color = lightColor;
    }

    public void IlluminateClick()
    {
        if(lightMaterial.IsKeywordEnabled("_EMISSION"))
        {
            lightMaterial.DisableKeyword("_EMISSION");
            lightMaterial.SetColor("_EmissionColor", lightColor);
            lightMaterial.globalIlluminationFlags = MaterialGlobalIlluminationFlags.EmissiveIsBlack;
            //mat.SetColor("_EmissionColor", Color.black);
            Debug.Log("Emisison disabled");
        }
        else
        {
            lightMaterial.EnableKeyword("_EMISSION");
            lightMaterial.SetColor("_EmissionColor", lightColor);
            Debug.Log("Emission enabled");
        }
    }

    /// <summary>
    /// Quick and dirty
    /// Attach the material to all the meshes listed in the list
    /// </summary>
    private void SetAllObjsColor()
    {
        foreach(GameObject go in sceneObjs)
        {
            go.GetComponent<Renderer>().material = lightMaterial;
        }
    }
}
