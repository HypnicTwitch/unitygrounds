﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickMove : MonoBehaviour {

    public int moveSpeed = 3;
    public GameObject terrain;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetMouseButtonUp(0)) {
            Ray clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (terrain.GetComponent<Collider>().Raycast (clickRay, out hit, Mathf.Infinity)) {
                transform.LookAt(hit.point);
            }
        }
	}
}
