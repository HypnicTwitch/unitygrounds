﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ColorChangeTextOnMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    //Singleton, because GetComponent is expensive and I only need to do it once.
    private Text myText;
    

    // Start is called before the first frame update
    void Start()
    {
        GetMyText().color = Color.blue;
    }

    private Text GetMyText()
    {
        if(myText == null)
        {
            myText = GetComponent<Text>();
        }
        return myText;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GetMyText().color = GetRandomColor();
    }

    public Color GetRandomColor()
    {
        float red = Random.Range(0.0f, 1.0f);
        float green = Random.Range(0, 1f);
        float blue = Random.Range(0, 1f);
        Color randomColor = new Color(red, green, blue, 1.0f);

        return randomColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetMyText().color = Color.white;
    }
}
