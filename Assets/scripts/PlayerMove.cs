﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public float moveSpeed = 1;
    public float jumpForce;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.A))
        {
            transform.position += new Vector3(-moveSpeed,0f,0f) * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.position += new Vector3(moveSpeed, 0f, 0f) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.position += new Vector3(0f, 0f, moveSpeed) * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.position += new Vector3(0f, 0f, -moveSpeed) * Time.deltaTime;
        }

        if(Input.GetButtonDown("Jump"))
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce);
        }

    }
}
