﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorChangeObjectOnMouseOver : MonoBehaviour
{

    [Tooltip("Color for non-hover state")]
    [SerializeField] private Color normalColor;

    [Tooltip("Color for hover state")]
    [SerializeField] private Color hoverColor;

    private Renderer myRenderer;

    private void Start()
    {
        myRenderer = GetComponent<Renderer>();

        if (normalColor == null)
        {
            normalColor = Color.gray;
        }

        if (hoverColor == null)
        {
            hoverColor = Color.blue;
        }

    }

    private void OnMouseEnter()
    {
        myRenderer.material.color = hoverColor;
    }

    private void OnMouseExit()
    {
        myRenderer.material.color = normalColor;
    }

}
