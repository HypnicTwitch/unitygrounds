# unitygrounds

Unity sandbox for learnin&#39;
### Authors
* Dave Stout
* Ben Carson
### Purpose
Unity sandbox for learnin'

An additional side effect of this repository is that everyone involved is learning git and bitbucket.

#### Quick Clarification on git merge conflict resolution
    <<<<<<<HEAD
    This line is the current branch
    =======
    This line is the data being merged in
    >>>>>>>[branch name or commit id]
